
const supertest = require("supertest");
const app=require("../index");

describe('Sample Test', () => {
    it('should test that true === true', () => {
      expect(true).toBe(true)
    })
  })


  test("Test para validar que la respuesta sea json", async () => {
    await supertest(app)
    .get("/productos-json")
    .expect(200)
    .then((response)=>{
      expect(response.status).toBe(200);
      expect(response.headers["content-type"]).toEqual(
        "application/json; charset=utf-8"
        );
       
    });
  });

  test("Longitud del array es correcta", async () => {
    await supertest(app)
    .get("/productos-json")
    .expect(200)
    .then((response)=>{
        const longitud=response.body.length;
        console.log(longitud);
        expect(response.headers["content-type"]).toEqual(
            "application/json; charset=utf-8"
        );

        expect(response.body).toHaveLength(7);
    })
  });

  test("El id del primer producto es 1", async () => {
    await supertest(app)
    .get("/productos-json")
    .expect(200)
    .then((response)=>{
        
        expect(response.headers["content-type"]).toEqual(
            "application/json; charset=utf-8"
        );

        expect(response.body[0]).toHaveProperty("id", 1);
    })
  });

test("El id del ultimo producto es 7", async () => {
    await supertest(app)
    .get("/productos-json")
    .expect(200)
    .then((response)=>{
        const longitud=response.body.length;
        expect(response.headers["content-type"]).toEqual(
            "application/json; charset=utf-8"
        );

        expect(response.body[longitud-1]).toHaveProperty("id", 7);
    })
  });
