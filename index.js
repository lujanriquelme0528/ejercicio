
const slugify= require("slugify");
const express= require ("express");

const app= express ();

const titulo= "Y12653";
app.set("view engine", "ejs");
app.set("views","views");
app.use(express.json());
app.use(express.urlencoded({ extended: true })); 

const productos=[
    {id:1, nombre:"Jabon Plusbelle", precio: 3500.1, slug:"peso, boton, dpi"},
    {id:2, nombre:"Fideo Caracol Anita", precio: 4500.3, slug:"peso, boton, dpi"},
    {id:3, nombre:"Galletita Aventura de Chocolate", precio: 3500.7, slug:"peso, botos, dpi"},
    {id:4, nombre:"Papel Higienico BOX", precio: 1500.1, slug:"peso, boes, dpi"},
    {id:5, nombre:"Yerba Campesino Menta y Boldo", precio: 6000.3, slug:"peso, bones, di"},
    {id:6, nombre:"Hielo", precio: 500.7, slug:"po, botones, dpi"},
    {id:7, nombre:"Coca Cola", precio: 15000.5, slug:"pso, botones, dpi"},
];

productos.forEach(productos => {
    productos.slug=slugify(productos.nombre);
});

app.get(("/bienvenida"), (req, res) => {res.render("bienvenida", {titulo:titulo, productos:productos})})

app.get("/det_producto/:slug", (req, res)=> {
    const slug= req.params.slug;
    const productoEncontrado=productos.find(producto=>producto.slug===slug)
    res.render("det_producto", {titulo:titulo, producto: productoEncontrado});
})


app.put("/det_producto/:slug", (req, res) => {
    const slug = req.params.slug;
    const productoEncontrado = productos.find(producto => producto.slug === slug);
  
    if (productoEncontrado) {
        productoEncontrado.nombre = req.body.nombre;
        productoEncontrado.precio = parseFloat(req.body.precio);
  
        productos.forEach(producto => {
          producto.slug = slugify(producto.nombre);
        });

        res.json({ success: true, slug: productoEncontrado.slug });
    } else {
        res.status(404).send("Producto no encontrado");
    }
});


app.delete("/det_producto/:slug", (req, res) => {
    const slug= req.params.slug;
    const elimProducto = productos.findIndex(producto => producto.slug === slug);

    if (elimProducto !== -1){
        productos.splice(elimProducto, 1);

        res.status(200).json({mensaje:"Producto eliminado"});
    }else{
        res.status(404).json({mensaje: "Producto no encontrado"});
    };
  
});

app.get("/productos-json", (req, res)=> {
    res.json(productos);
});



app.get("/crear_producto", (req, res) => {
    res.render("crear_producto", { titulo: titulo });
});

app.post("/crear_producto", (req, res) => {
     
    const nuevoProducto = {
        id: (Math.max(...productos.map((producto)=>producto.id))+1),
       
        nombre: req.body.nombre,
        precio: parseFloat(req.body.precio),
        slug: slugify(req.body.nombre)
    };

    productos.push(nuevoProducto);

    res.redirect("/bienvenida");
});

module.exports = app;
