# primer_ejercicio_riquelmelorena

Guia de funcionamiento del trabajo

- Primeramente debemos levantar el servicio en la terminal "npm run dev"

- Luego entrar en el navegador de su preferencia con la siguiente ruta "http://localhost:3000/bienvenida"

- Se visualiza en la pantalla principal un listado de los articulos o productos que se encuentran en una despensa o kiosko. Estos registros pueden ser editados o eliminados; como asi tambien se pueden agregar nuevos articulos. 

- En cada registro hay un enlace que nos redirije a una nueva pantalla para realizar dichos procesos mencionados arriba. Posterior a esto una vez realizado el cambio o se haya agregado un nuevo producto, al dar click sobre el boton "crear producto" nos dirije a la pantalla principal nuevamente con el listado actualizado.

- Si se Elimina te salta una pequeña ventana preguntandote si estas seguro de eliminar dicho registro. Al darle si, se borra y vuelve a la pagina principal sin dicho registro

:)